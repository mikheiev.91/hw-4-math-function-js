"use strict";

	const checkNum = (num) => isNaN(num) || num === "" || num === null;

	function isOperationValid (operator) {
		if (operator === '+' 
		|| operator === '-' 
		|| operator === '*' 
		|| operator === '/') {
			return true;
		}
		return false;
	}

	let number1 = 
	prompt('Please enter first number (only numbers allowed!):');

	let number2 = 
	prompt('Please enter second number (only numbers allowed!):');

	// if (checkNum(number1) || checkNum(number2)) {
	// 	alert("Error! Please enter ONLY numbers!");

		while (checkNum(number1) || checkNum(number2)) {
			alert("Error! Please enter ONLY numbers!");
			number1 = 
			prompt('Please enter first number (only numbers allowed!):');
			number2 = 
			prompt('Please enter second number (only numbers allowed!):');
		}

	number1 = Number(number1);
	number2 = Number(number2);

	let mathOperation = 
	prompt('Please enter operation (ONLY +, -, *, /. allowed!) :');

	if (!isOperationValid(mathOperation)) {
		while(!isOperation(mathOperation)) {
			mathOperation = 
			prompt('Please enter operation (ONLY +, -, *, /. allowed!) :');
		}
	}

	function calculator(num1, num2, operation) {
		if (mathOperation === '+') {
			console.log(number1 + number2);
			return number1 + number2;
		} else if (mathOperation === '-') {
			console.log(number1 - number2);
			return number1 - number2;
		} else if (mathOperation === '*') {
			console.log(number1 * number2);
			return number1 * number2;
		} else if (mathOperation === '/') {
			console.log(number1 / number2);
			return number1 / number2;
		} 
	}

	calculator(number1, number2, mathOperation);